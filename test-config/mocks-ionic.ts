import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PokedexServiceProvider } from '../src/providers/pokedex-service/pokedex.service';
import { Observable } from 'rxjs';
import { PokemonList } from '../src/interfaces/pokemon-list.interface';
import { Pokemon, TypeObject, PokemonType } from '../src/interfaces/pokemon.class';
import { PokemonSpecies, FlavorTextEntry } from '../src/interfaces/pokemon-species.interface';
import { PokemonMove, PokemonMoveList } from '../src/interfaces/pokemon-move.interface';

export class PlatformMock {
  public ready(): Promise<string> {
    return new Promise((resolve) => {
      resolve('READY');
    });
  }

  public getQueryParam() {
    return true;
  }

  public registerBackButtonAction(fn: Function, priority?: number): Function {
    return (() => true);
  }

  public hasFocus(ele: HTMLElement): boolean {
    return true;
  }

  public doc(): HTMLDocument {
    return document;
  }

  public is(): boolean {
    return true;
  }

  public getElementComputedStyle(container: any): any {
    return {
      paddingLeft: '10',
      paddingTop: '10',
      paddingRight: '10',
      paddingBottom: '10',
    };
  }

  public onResize(callback: any) {
    return callback;
  }

  public registerListener(ele: any, eventName: string, callback: any): Function {
    return (() => true);
  }

  public win(): Window {
    return window;
  }

  public raf(callback: any): number {
    return 1;
  }

  public timeout(callback: any, timer: number): any {
    return setTimeout(callback, timer);
  }

  public cancelTimeout(id: any) {
    // do nothing
  }

  public getActiveElement(): any {
    return document['activeElement'];
  }
}

export class StatusBarMock extends StatusBar {
  styleDefault() {
    return;
  }
}

export class SplashScreenMock extends SplashScreen {
  hide() {
    return;
  }
}

export class CacheServiceMock {
    setDefaultTTL() {
        return 0;
    }
    setOfflineInvalidate() {
        return;
    }
}

export class PokedexServiceProviderMock {
    public testTypeObj: TypeObject = {
        name:"Grass",
    }
    public testFlavorText: FlavorTextEntry = {
        flavor_text: "This is text flavor text",
        language: { name: "en", url: "www.test.com"},
        version: { name: "", url: ""},
    }
    public testSpecies: PokemonSpecies = {
        flavor_text_entries: [],
        genera: [],
    }
    public testTypes: PokemonType = {
        slot: 1,
        type: this.testTypeObj,
    }
    public testPokemon: Pokemon = {
        id: 1,
        name: "Bulbasaur",
        showPicture: false,
        types: [this.testTypes],
    }
    public testPokemonList: PokemonList = {
        count: 1,
        next: "",
        previous: true,
        results: [this.testPokemon],
    }

    public testPokemonMove: PokemonMove = {
        name: 'pound',
        url: '',
    }

    public testPokemonMoveList: PokemonMoveList = {
        count: 1,
        next: "",
        previous: null,
        results: [this.testPokemonMove],
    }
    getAllPokemon() {
        return Observable.of(this.testPokemonList);
    }
    getPokemonByIndex() {
        return Observable.of(this.testPokemon);
    }
    getPokemonBioByIndex() {
        this.testSpecies.flavor_text_entries.push(this.testFlavorText);
        return Observable.of(this.testSpecies);
    }

    getAllPokemonMoves() {
        return Observable.of(this.testPokemonMoveList);
    }
}

export class NavMock {
 
  public pop(): any {
    return new Promise(function(resolve: Function): void {
      resolve();
    });
  }
 
  public push(): any {
    return new Promise(function(resolve: Function): void {
      resolve();
    });
  }
 
  public getActive(): any {
    return {
      'instance': {
        'model': 'something',
      },
    };
  }
 
  public setRoot(): any {
    return true;
  }

  public registerChildNav(nav: any): void {
    return ;
  }

}

export class NavParamsMock {
    static returnParam = null;
    public get(key): any {
      if (NavParamsMock.returnParam) {
         return NavParamsMock.returnParam
      }
      return 'default';
    }
    static setParams(value){
      NavParamsMock.returnParam = value;
    }
}

export class ViewControllerMock { 

    public index: number = 0;

    public readReady = {
        subscribe() {

        }
    };
    public writeReady = {
        subscribe() {

        }
    };

    dismiss() {
        console.log('View Controller Dismiss Called');
    }

    public remove(startIndex: number): any { return {} };
    public _setHeader(): any { return {} }; 
    public _setIONContent(): any { return {} }; 
    public _setIONContentRef(): any { return {} }; 
    public _setNavbar() : any{ return{} };
}

export class DeepLinkerMock {

}